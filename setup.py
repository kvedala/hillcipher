from setuptools import setup
from setuptools import Extension
from Cython.Build import cythonize
from Cython.Compiler import Options
import numpy, os

# os.environ['CC'] = 'gcc-mp-9'
# os.environ['CXX'] = 'g++-mp-9'
os.environ['CC'] = 'clang-mp-8.0'
os.environ['CXX'] = 'clang++-mp-8.0'

Options.cimport_from_pyx = True
Options.annotate = True
Options.embed_pos_in_docstring = True

ext = [
    Extension(
        "HillCipher.*",
        sources=["HillCipher/*.pyx"],
        include_dirs=[numpy.get_include()],
        extra_compile_args=["-fopenmp", "-flto", "-Ofast", "-march=native"],
        extra_link_args=["-fopenmp", "-flto", "-march=native"],
        c_string_encoding='ascii'
    )
]

setup(
    name="HillCipher",
    version="1.0.0",
    author="Krishna Vedala",
    url="https://gitlab.com/kvedala/hillcipher",
    author_email="incoming+kvedala-hillcipher-15043042-issue-@incoming.gitlab.com",
    ext_modules=cythonize(ext, language_level=3),
    packages=["HillCipher",],
    package_data={
        "HillCipher": ['*.pxd'],
    },
    install_requires=['numpy'],
    python_requires='>=3',
    include_package_data=True,
    zip_safe=False,
)
