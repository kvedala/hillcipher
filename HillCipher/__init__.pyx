#!python
# cython: boundscheck=False
# cython: wraparound=False
# cython: nonecheck=False
# cython: cdivision=True
# cython: language_level=3
# cython: c_string_encoding=ascii
# cython: c_string_type=unicode, c_string_encoding=ascii
# distutils: extra_compile_args=-fopenmp -Wall -Ofast

from .HillCipher cimport HillCipher, get_matrix
from .HillCipher import HillCipher, get_matrix, gcd_c
