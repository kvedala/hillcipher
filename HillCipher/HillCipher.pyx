#!python
# cython: embedsignature=True
# cython: boundscheck=False
# cython: wraparound=False
# cython: nonecheck=False
# cython: cdivision=True
# cython: fast_gil=True
# cython: language_level=3
# cython: c_string_type=unicode, c_string_encoding=ascii
# distutils: extra_compile_args=-fopenmp -Wall -Ofast


cimport cython
from cython.parallel cimport prange
# cimport numpy as np
import numpy as np

import pickle
from libc.math cimport round
from libc.stdio cimport printf
from libc.stdlib cimport rand, abort
from libc.string cimport strlen, strcpy, memset, memcpy
cdef extern from "stdlib.h":
    double RAND_MAX
from cpython.mem cimport PyMem_RawMalloc, PyMem_RawRealloc, PyMem_RawFree

cdef const char *STRKEY = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~!@#$%^&*()_+`-=[]\{}|;':\",./<>?\r\n "
cdef unsigned int STRKEY_LEN = strlen(STRKEY)

cdef np.int64_t randrange(np.int64_t a, np.int64_t b) nogil:
    """
    Function to generate a random integer in a given range.
    
    Parameters
    ------------
    a : numpy.int64
    b : numpy.int64
    
    Returns
    ------------
    numpy.int64
        random integer between `a` and `b`
    """
    cdef long double r = <long double> rand() / RAND_MAX
    r = r * (b - a) + a 
    return <np.int64_t> r


cdef np.int64_t gcd(np.int64_t a, np.int64_t b) nogil:
    """
    Function to compute the GCD of two numbers. Used to obtain invertible encryption matrix. 
    Operates on 64-bit signed integers using the iterative version of Eucledean Algorithm.
    
    Parameters
    ------------
    a : numpy.int64
    b : numpy.int64
    
    Returns
    ------------
    numpy.int64
        GCD of `a` and `b`
    """
    if b > a:        # make sure that a > b
        a, b = b, a
        
    while b != 0:
        a, b = b, a % b
    return a


def gcd_c(np.int64_t a, np.int64_t b):
    """
    Python interface to the C-function to compute the GCD of two numbers. Used to obtain invertible encryption matrix. 
    Operates on 64-bit signed integers using the iterative version of Eucledean Algorithm.
    
    Parameters
    ------------
    a : numpy.int64
    b : numpy.int64
    
    Returns
    ------------
    numpy.int64
        GCD of `a` and `b`
    """
    return gcd(a, b)

            
cdef inline np.uint64_t modulo(np.int64_t N, np.int64_t base=STRKEY_LEN) nogil:
    """
    Perform `modulo` operation in C. The `%` operator in C is a reminder
    function and not modulus like in python. 
    
    Parameters
    -------------
    N : numpy.int64_t
        input number to take modulo of
    base : numpy.int64_t (optional, default=STRKEY_LEN)
        base to which to obtain modulus
    
    Returns
    -------------
    numpy.int64_t
        Modulus of `N` to the `base`
    """
    cdef np.int64_t ret = N % base # reminder operation of C
    if ret >= 0:
        return ret 
    else:
        return ret + base



@cython.final
cdef class HillCipher:
    """
    Class to perform encryption and decryption using [https://en.wikipedia.org/wiki/Hill_cipher](Hill cipher) algorithm.
    """
#     cdef:
#         char *key_string
#         np.int32_t[:,:] encrypt_key
#         np.int32_t[:,:] decrypt_key
#         int key_len, break_key
#         np.int64_t det
        
    def __init__(self, np.ndarray[np.int32_t,ndim=2] encrypt_key):
        """
        Initialize Hill Cipher encrypt-decrypt object with an initial encryption matrix.
        Automatically computes the corresponding decryption matrix.
        
        Parameters
        ------------
        encrypt_key : numpy.ndarray[numpy.int32_t,ndim=2]
            encryption matrix
        """
        cdef np.int64_t det_gcd, i, j
        self.key_string = STRKEY
        self.key_len = strlen(STRKEY)
        self.break_key = encrypt_key.shape[0]
        self.encrypt_key = encrypt_key.copy()
        for i in prange(self.break_key, nogil=True):
            for j in prange(self.break_key):
                self.encrypt_key[i,j] = modulo(self.encrypt_key[i,j])
                
        self.check_determinant(&det_gcd)
        if det_gcd != 1:
            raise ValueError("discriminant modular {} of encryption key({}) is not co prime w.r.t {}.\nTry another key.".format(
                        self.key_len, self.det, self.key_len))
        self.decrypt_key = encrypt_key.copy()
        self.make_decrypt_key()
        
    
    @classmethod
    def from_file(self, fname: str):
        """
        Create a new cipher object from a saved file. 
        
        Parameters
        -------------
        fname : str
            name of file to load from
        """
        with open(fname, "rb") as fp:
            return pickle.load(fp)
    
    
    def save(self, fname: str):
        """
        Save the current cipher object to file. 
        
        Parameters
        -------------
        fname : str
            name of file to save to
        """
        with open(fname, "wb") as fp:
            pickle.dump(self, fp, 3)


    def __reduce__(self):
        '''Define how instances of HillCipher are pickled.'''
        return self.__class__, (self.encrypt_key.base,)
    

    def __setstate__(self, a):
        '''Define how instances of HillCipher are restored.'''
        self.break_key = a.shape[0]
        self.encrypt_key = a.copy()
        self.decrypt_key = a.copy()
        self.make_decrypt_key()

    
    cdef void check_determinant(self, np.int64_t *r_gcd):
        """
        Check if the determinant of encryption matrix is invertible.
        
        Parameters
        ------------
        r_gcd : *numpy.int64_t
            Pointer to store gcd of determinant result in.
            Should be equal to '1' for invertible matrix.
        """
        cdef long tmp
        self.det = <np.int64_t> round(np.linalg.det(self.encrypt_key))

        with nogil:
            tmp = modulo(self.det) if self.det < 0 else self.det
            r_gcd[0] = gcd(tmp, self.key_len)
    

    cdef void process_text(self, const np.uint8_t[:] in_text, np.uint8_t *out_text):
        """
        Convert input text to have a length to be a multiple of key length.
        
        Parameters
        ------------
        in_text : bytearray
            ascii decoded bytes array
        out_text: *numpy.int8_t
            pointer to chaarcter array to store processed text in
        """
        cdef:
            long L = in_text.shape[0] 
            int M = self.encrypt_key.shape[0]
            long L2 = L if L % M == 0 else L + M - (L % M)
            long i
            np.uint8_t last = in_text[L-1]
    
        with nogil:
            strcpy(<char*> out_text, <const char*> &in_text[0])
            for i in range(L, L2):
                out_text[i] = last
    

    cdef inline np.uint8_t get_char_index(self, const char ch) nogil:
        """
        Return the index of ASCII character in the key.
        
        Parameters
        ------------
        ch : char
            character to check
            
        Return
        ------------
        numpy.uint8
            index of character
        
        See Also
        ------------
        HillCipher.get_index_char
        """
        cdef np.uint8_t idx
        for idx in range(self.key_len):
            if self.key_string[idx] == ch:
                return idx
        raise ValueError("Should not be seeing this! Probably unknown character present: '{}'".format(ch))

    
    cdef inline char get_index_char(self, const np.uint8_t idx) nogil:
        """
        Return the character corresponding to index of ASCII character in the key.
        
        Parameters
        ------------
        idx : numpy.uint8
            index to check
            
        Return
        ------------
        char
            character corresponding to the index
        
        See Also
        ------------
        HillCipher.get_char_index
        """
        return self.key_string[idx]

    
    cdef void mat_mul(self, const np.uint8_t[:] vec, np.uint8_t[:] out, np.uint8_t to_encode):
        """
        Perform vector multiplication of a vector with the encryption key.
        
        Parameters
        ------------
        vec : numpy.uint8[:]
            memoryview of vector to multiply
        out : numpy.uint8[:]
            memoryview of vector to store the result in
        to_encode : numpy.uint8
            flag to perform encoding (=0) or decoding (=1)
        """
        cdef:
            int i, j
            np.int64_t tmp
        if to_encode > 1:
            raise ValueError("'to_encode={}' should be 0 (encode) or 1 (decode)".format(to_encode))
    
        for i in range(self.break_key):
            tmp = 0
            for j in range(self.break_key):
                if to_encode == 0:
                    tmp += self.encrypt_key[i,j] * vec[j]
                elif to_encode == 1:
                    tmp += self.decrypt_key[i,j] * vec[j]
            out[i] = <np.uint8_t> (tmp % self.key_len)
            
            
    cpdef double index_of_coincidence(self, const np.uint8_t[:] text):
        cdef:
            np.int64_t i, idx, L = text.shape[0]
            np.int64_t num = 0, deno = L * (L - 1)
            unsigned char ch
            np.int32_t *C = <np.int32_t*> PyMem_RawMalloc(self.key_len * sizeof(np.int32_t))
            
        memset(C, 0, self.key_len * sizeof(np.int32_t))
        
        for i in range(L):
            ch = text[i]
            idx = self.get_char_index(ch)
            C[idx] += 1
        
        for i in range(self.key_len):
            num += C[i] * (C[i] - 1)

        try:
            return <double> num / (L * (L - 1))
        finally: 
            PyMem_RawFree(C)
    
    
    cpdef str codec(self, const np.uint8_t[:] text, np.int8_t type):
        """
        Perform encoding and/or decoding using Hill Cipher algorithm.
        
        Parameters
        ------------
        text : bytearray
            ascii decoded bytes array
        to_encode : numpy.uint8
            flag to perform encoding (=0) or decoding (=1)
            
        Return 
        ------------
        str
            encoded/decoded string
        
        See Also
        ------------
        
        """
        cdef:
            long L = text.shape[0] # strlen(text)
            int M = self.break_key
            long L2 = L if L % M == 0 else L + M - (L % M)
            int i, j
            np.uint8_t *_tmp = <np.uint8_t*> PyMem_RawMalloc(L2)
            np.ndarray[ndim=1,dtype=np.uint8_t] _batch_int = np.ndarray(self.break_key, dtype=np.uint8)
            np.uint8_t[:] batch_int = _batch_int
            np.ndarray[ndim=1,dtype=np.uint8_t] batch_encrypt = np.ndarray(self.break_key, dtype=np.uint8)
            np.uint8_t[:] _batch_encrypt = batch_encrypt
            char *encrypted_txt = <char*> PyMem_RawMalloc(L2)
            np.uint8_t *tmp2
        
        if _tmp is NULL or encrypted_txt is NULL:
            raise MemoryError("codec: Unable to allocate memory.")
        
        memset(encrypted_txt, b'\0', L2)
        
        try:
            self.process_text(text, _tmp)
            for i in range(0, L2 - self.break_key + 1, self.break_key):
                for j in range(self.break_key):
                    batch_int[j] = self.get_char_index(_tmp[i + j])
                
                self.mat_mul(batch_int, _batch_encrypt, type)
                for j in range(self.break_key):
                    encrypted_txt[i+j] = self.get_index_char(_batch_encrypt[j])
        
            encrypted_txt[L2] = b'\0'
            return <str> encrypted_txt
        finally:
            PyMem_RawFree(_tmp)
            PyMem_RawFree(encrypted_txt) 


    cdef void make_decrypt_key(self):
        """
        Compute the decryption key matrix.
        """
        cdef np.int8_t i, j, det_inv = -1
        cdef np.int64_t tmp_val
        cdef long det_tmp = self.det % self.key_len if self.det < 0 else self.det
        cdef double[:,:] tmp = np.linalg.inv(self.encrypt_key)
        
        for i in range(self.key_len):
            if modulo(det_tmp * i) == 1:
                det_inv = i
                break
    
        if det_inv == -1:
            raise ValueError("could not find a co-prime for inversion")
        
        det_tmp = det_inv * self.det
        
        for i in range(self.break_key):
            for j in range(self.break_key):
                tmp_val = <np.int64_t> round(tmp[i,j] * det_tmp)
                self.decrypt_key[i,j] = modulo(tmp_val)

                
    cpdef str encode(self, str text, bint debug=False):
        """
        Perform encoding using Hill Cipher algorithm.
        
        Parameters
        ------------
        text : bytearray
            ascii decoded bytes array
            
        Return 
        ------------
        str
            encoded string
        
        See Also
        ------------
        
        """
        cdef const np.uint8_t[:] txt = bytes(text, 'ascii')
        cdef str out = self.codec(txt, 0)
        cdef double ioc1
        cdef double ioc2
        if debug:
            ioc1 = self.index_of_coincidence(txt)
            ioc2 = self.index_of_coincidence(bytes(out, 'ascii'))
            print("Index of coincidence: {:.4g} -> {:.4g}".format(ioc1, ioc2))

        return out


    cpdef str decode(self, str text):
        """
        Perform decoding using Hill Cipher algorithm.
        
        Parameters
        ------------
        text : bytearray
            ascii decoded bytes array
        debug : boolean (default=False)
            whether to print index of coincidence values
            
        Return 
        ------------
        str
            decoded string
        
        See Also
        ------------
        
        """
        cdef np.uint8_t[:] txt = bytes(text, 'ascii')
        cdef str out = self.codec(txt, 1)
            
        return out
    
    
    cpdef str encode_file(self, str path, bint debug=False):
        """
        Encrypt a file using Hill Cipher algorithm.
        
        Parameters
        ------------
        path : str
            path of the file to encrypt
        debug : boolean (default=False)
            whether to print index of coincidence values
            
        Return 
        ------------
        str
            encrypted string
        
        See Also
        ------------
        decode_file, encode_file_save
        
        """
        cdef str encoded
        cdef double ioc1, ioc2
        try:
            with open(path, "rb") as fp:
                txt = fp.read()
                encoded = self.codec(txt, 0)
            
            if debug:
                ioc1 = self.index_of_coincidence(txt)
                ioc2 = self.index_of_coincidence(bytes(encoded, 'ascii'))
                
        except Exception as e:
            print("Exception ('{}'): {}".format(path, e))
        else:
            print("Succesfully encoded file: " + path)
            if debug:
                print("Index of coincidence: {:.4g} -> {:.4g}".format(ioc1, ioc2))
        
        return encoded
            
    
    cpdef str encode_file_save(self, str in_path, str out_path, bint debug=False):
        """
        Encrypt a file using Hill Cipher algorithm and save the 
        encrypted data to another file.
        
        Parameters
        ------------
        in_path : str
            path of the file to encrypt
        out_path : str
            path of the file save encrypted file
        debug : boolean (default=False)
            whether to print index of coincidence values
        
        See Also
        ------------
        decode_file, encode_file
        
        """
        cdef double ioc1, ioc2
        cdef str encoded
        try:
            with open(in_path, "rb") as fp:
                txt = fp.read()
                encoded = self.codec(txt, 0)
                
            if debug:
                ioc1 = self.index_of_coincidence(txt)
                ioc2 = self.index_of_coincidence(bytes(encoded, 'ascii'))
                
            with open(out_path, "wb") as fp:
                fp.write(bytes(encoded, 'ascii'))

        except Exception as e:
            print("Exception ('{}'): {}".format(fp.name, e))
        else:
            print("Succesfully encoded file: " + in_path + " -> " + out_path)
            if debug:
                print("Index of coincidence: {:.4g} -> {:.4g}".format(ioc1, ioc2))
        
        return encoded
                  
    
    cpdef str decode_file(self, str path):
        """
        Decrypt a file using Hill Cipher algorithm.
        
        Parameters
        ------------
        path : str
            path of the file to decrypt
            
        Return 
        ------------
        str
            decrypted string
        
        See Also
        ------------
        encode_file, decode_file_save
        """
        cdef str decoded
        try:
            with open(path, "rb") as fp:
                txt = fp.read()
                decoded = self.codec(txt, 1)
        except Exception as e:
            print("Exception ('{}'): {}".format(path, e))
        else:
            print("Succesfully decoded file: " + path)
            return decoded
            
    
    cpdef str decode_file_save(self, str in_path, str out_path):
        """
        Decrypt a file using Hill Cipher algorithm and save the 
        decrypted data to another file.
        
        Parameters
        ------------
        in_path : str
            path of the file to decrypt
        out_path : str
            path of the file save decrypted file
        
        See Also
        ------------
        decode_file, encode_file
        
        """
        cdef str decoded
        try:
            with open(in_path, "rb") as fp:
                txt = fp.read()
                decoded = self.codec(txt, 1)
            
            with open(out_path, "wb") as fp:
                fp.write(bytes(decoded, 'ascii'))
        except Exception as e:
            print("Exception ('{}'): {}".format(fp.name, e))
        else:
            print("Succesfully decoded file: " + in_path + "->" + out_path)
        
        return decoded
        

cpdef get_matrix(int min_int=0, int max_int=100, 
                 np.uint64_t MAX_TRIES=<np.int64_t>1e5, 
                 np.uint64_t MAX_DET=1<<40, unsigned int size=10):
    """
    Function to obtain a random square matrix for use in 
    HillChipher. This matrix must be invertible in modulo 
    97, all elements must be modulo 97 with smallest 
    determinant to ensure easy and reliable computations. 
    
    Parameters
    ------------
    min_int: int (default=0)
        Minimum value of matrix element
    max_int: int (default=100)
        Maximum value of matrix element
    MAX_TRIES: np.uint64_t (default=1e5)
        Maximum attempts to obtain the random matrix
    MAX_DET: np.uint64_t (default=2**40)
        Maximum absolute value of matrix determinant
    size: unsigned int (default=10)
        Shape of the equare matrix
    
    Returns
    ------------
    int
        number of attempts made
    int
        determinant value
    int 
        gcd(determinant, 97)
    numpy.ndarray[dtype=numpy.int32]
        output numpy matrix
        
    Example
    --------------
    >>> tries, det, gcd, a = get_matrix(0, 10, max_tries, max_det, 10)
    """
    cdef:
        np.uint64_t tries = 0, pick = 0
        int modulus = STRKEY_LEN
        np.uint64_t min_det2 = MAX_DET
        np.ndarray[np.int16_t, ndim=2] a, min_a
        np.int64_t dd, dd2, g, min_det, min_gcd
        
    for tries in range(MAX_TRIES):
        a = np.random.randint(min_int, max_int, size=(size,size), dtype=np.int16)
#         det = linalg.det(a)
        dd = dd2 = <np.int64_t>round(np.linalg.det(a % modulus))
        if abs(dd2) > MAX_DET or dd2 == 0:
            continue
        elif dd < 0:
            dd = modulo(dd, modulus)
        g = gcd(dd, modulus)
        if abs(dd2) < abs(min_det2) and g == 1:
            pick += 1
            min_det = dd
            min_det2 = dd2
            min_a = a
            min_gcd = g
#             print(min_det)
    if abs(min_det2) >= MAX_DET:
        print("Unable to find a solution: ")
        return tries, None, None, None
    
    print("{}/{} \t {:g}\t {:g} <> {:g}\t gcd: {}\n {}".format(pick, tries, min_det2, min_det, MAX_DET, min_gcd, min_a))
    return pick, min_det, min_gcd, min_a.astype(np.int32)

