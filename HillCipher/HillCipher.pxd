#!python

cimport numpy as np

cdef public np.int64_t randrange(np.int64_t a, np.int64_t b) nogil

cdef public np.int64_t gcd(np.int64_t a, np.int64_t b) nogil

cdef public np.uint64_t modulo(np.int64_t N, np.int64_t base=?) nogil

cpdef public get_matrix(int min_int=*, int max_int=?, 
                 np.uint64_t MAX_TRIES=?, 
                 np.uint64_t MAX_DET=?, unsigned int size=?)

cdef class HillCipher:
    cdef:
        char *key_string
        np.int32_t[:,:] encrypt_key
        np.int32_t[:,:] decrypt_key
        int key_len, break_key
        np.int64_t det
    
    cdef void check_determinant(self, np.int64_t *r_gcd)

    cdef void process_text(self, const np.uint8_t[:] in_text, np.uint8_t *out_text)

    cdef inline np.uint8_t get_char_index(self, const char ch) nogil
    
    cdef inline char get_index_char(self, const np.uint8_t idx) nogil
    
    cdef void mat_mul(self, const np.uint8_t[:] vec, np.uint8_t[:] out, np.uint8_t to_encode)
    
    cpdef double index_of_coincidence(self, const np.uint8_t[:] text)
    
    cpdef str codec(self, const np.uint8_t[:] text, np.int8_t type)
            
    cdef void make_decrypt_key(self)
        
    cpdef str encode(self, str txt, bint debug=*)
    
    cpdef str decode(self, str txt)
    
    cpdef str encode_file(self, str path, bint debug=*)
    
    cpdef str encode_file_save(self, str in_path, str out_path, bint debug=*)
    
    cpdef str decode_file(self, str path)
    
    cpdef str decode_file_save(self, str in_path, str out_path)